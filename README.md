# QR Screenshot
Simple script to select screenshot a QR code on screen and scan it.

## Installation
Install the following packages with your package manager:

- gnome-screenshot
- qtqr

Then simply run the isntall script with `./install.sh`.

If you get a permission error run `chmod +x install.sh` first.

## Usage
Simply run the QR Screenshot application from your start menu and select the QR code on screen.

![Your browser doesn't support animated WebP](usage.webp)
